SUMMARY = "Library to manipulate zoned block devices"
DESCRIPTION = "libzbd uses the kernel provided zoned block device interface \
based on the ioctl() system call. It provides functions for discovering and \
managing the state of zones of zoned block devices. Read and write accesses \
to the devices can be done using standard standard I/O system calls."
HOMEPAGE = "https://zonedstorage.io/projects/libzbd"

LICENSE = "GPL-3.0-or-later & LGPL-3.0-or-later & CC0-1.0"
LIC_FILES_CHKSUM = "file://LICENSES/CC0-1.0.txt;md5=65d3616852dbf7b1a6d4b53b00626032 \
                    file://LICENSES/GPL-3.0-or-later.txt;md5=1ebbd3e34237af26da5dc08a4e440464 \
                    file://LICENSES/CC0-1.0.txt;md5=65d3616852dbf7b1a6d4b53b00626032 \
                   "
SRC_URI = "git://github.com/westerndigitalcorporation/libzbd;branch=master;protocol=https"

SRCREV = "1e624fa5fd4ef3cc32a3d912b6789eb61f33a24a"

S = "${WORKDIR}/git"

DEPENDS = " \
            ${@bb.utils.contains_any('DISTRO_FEATURES', '${GTK3DISTROFEATURES}', 'gtk+3', '', d)} \
          "
inherit pkgconfig autotools-brokensep

FILES:${PN} += "${@bb.utils.contains_any('DISTRO_FEATURES', '${GTK3DISTROFEATURES}', '${datadir}/polkit-1/actions/', '', d)}"
