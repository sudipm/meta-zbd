This README file contains information on the contents of the meta-zbd layer.

Please see the corresponding sections below for details.

Dependencies
============

```
  URI: git://git.openembedded.org/openembedded-core                      
  branch: master                          
  revision: HEAD                              
```

If `x11` or `wayland` is enabled in DISTRO_FEATURES then it will also build the graphical tools.

Patches
=======

Please submit any patches against the meta-zbd layer as
[Merge Requests](https://gitlab.com/sudipm/meta-zbd/merge_requests)

Maintainer: Sudip Mukherjee <sudipm.mukherjee@gmail.com>

Table of Contents
=================

   I. Adding the meta-zbd layer to your build                            
  II. Misc                            

I. Adding the meta-zbd layer to your build
=================================================

Run 'bitbake-layers add-layer meta-zbd'

Add the recipe `libzbd` to the final image file or in the `local.conf`

```
IMAGE_INSTALL:append = "libzbd"
```

II. Misc
========

More details of `Zoned Storage` is available at https://zonedstorage.io/docs/introduction                         
This recipe will enable `null_blk` kernel block device driver which is the easiest method                     
to emulate a zoned block device. Emulated zoned block devices make it possible to do                           
application development and kernel tests even if there is no zoned storage hardware.                          


License
=======

All metadata is MIT licensed unless otherwise stated. Source code and                          
binaries included in tree for individual recipes is under the LICENSE                        
stated in each recipe (.bb file) unless otherwise stated.                          
